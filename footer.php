		</div>
		
		<script src="/js/vendor/jquery-1.9.1.min.js"></script>
		
		<!--[if !IE]><!-->
       
        <script src="/js/vendor/three.min.js"></script>
        <!--<script src="js/points.js"></script>-->
        
        <script src="/js/obj/bird.js"></script>
        <script src="/js/boid.js"></script>
        <script src="/js/init.js"></script>

		<!--<![endif]-->

		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		
		  ga('create', 'UA-42176212-1', 'bramloosveld.be');
		  ga('send', 'pageview');
		
		</script>	

    </body>
</html>