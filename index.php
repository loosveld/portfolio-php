<?php include('header.php'); ?>
			
	<div class="content">
		<div class="icon"><img src="img/icon.png" title="Bram"/></div>
		<div class="name">Bram Loosveld</div>
		<div class="short">Freelance Digital Designer / Coder</div>
		<div class="links"></div>
	</div>
	<div class="icons">
		<a href="mailto:bramloosveld@gmail.com" id="mail" target="_blank"></a>
		<a href="http://www.linkedin.com/profile/view?id=11834342" id="linkedin" target="_blank"></a>
		<a href="https://twitter.com/crispclean" id="twitter" target="_blank"></a>
	</div>
	<canvas width="100%" height="100%"></canvas>
			
<?php include('footer.php'); ?>
