<?php include('../header.php'); ?>
<style>
	body{
		overflow-y: auto;
	}
</style>

<div class="content" style="height:auto;top:0px;margin-top:100px">

	<div class="icon"><a href="/"><img src="/img/icon.png" title="Bram"/></a></div>
	<div class="name">Portfolio</div>

	<section class="project">
			<figure><img src="sdl.png"></figure>           
	    <div class="content">
	    
	    	<h4>SDL</h4> 
	    	<i>June 2015</i><br><br>
	    	Update current website
	    	<br><br>
	    	<ul>
	    	<li>UX Design</li>
	    	<li>Clientside Development<br>(HTML, CSS, AngularJS, Sass, Grunt, Bower, Kirby)</li>
	    	</ul>
	    	  
	    </div>
	    <br style="clear:left">
	</section>


	<section class="project">
			<figure><img src="lionel.png"></figure>           
	    <div class="content">
	    
	    	<h4>Lionel Bajart</h4> 
	    	<i>May 2015</i><br><br>
	    	Personal website
	    	<br><br>
	    	<ul>
	    	<li>UX Design</li>
	    	<li>Clientside Development<br>(HTML, CSS, AngularJS, Sass, Grunt, Bower, Kirby)</li>
	    	</ul>
	    	  
	    </div>
	    <br style="clear:left">
	</section>

	<section class="project">
			<figure><img src="mse.png"></figure>           
	    <div class="content">
	    
	    	<h4>MSE</h4> 
	    	<i>May 2015</i><br><br>
	    	Coorporate website 
	    	<br><br>
	    	<ul>
	    	<li>UX Design</li>
	    	<li>Clientside Development<br>(HTML, CSS, AngularJS, Sass, Grunt, Bower, Kirby)</li>
	    	</ul>
	    	  
	    </div>
	    <br style="clear:left">
	</section>

	<section class="project">
			<figure><img src="bizzmine-2.png"></figure>           
	    <div class="content">
	    
	    	<h4>Bizzmine</h4> 
	    	<i>January 2015</i><br><br>
	    	Online ERP webtool
	    	<br><br>
	    	<ul>
	    	<li>UX Design</li>
	    	<li>Clientside Development<br>(HTML, CSS, AngularJS, Sass, Grunt)</li>
	    	</ul>
	    	  
	    </div>
	    <br style="clear:left">
	</section>

	<section class="project">
			<figure><img src="hostbasket-1.png"></figure>           
	    <div class="content">
	    
	    	<h4>Telenet</h4> 
	    	<i>November 2014</i><br><br>
	    	Hostbasket/Telenet Hosting marketing website 
	    	<br><br>
	    	<ul>
	    	<li>Design</li>
	    	<li>Clientside Development<br>(HTML, CSS, AngularJS, Sass, Grunt)</li>
	    	</ul>
	    	  
	    </div>
	    <br style="clear:left">
	</section>

	<section class="project">
			<figure><img src="hostbasket-2.png"></figure>           
	    <div class="content">
	    
	    	<h4>Telenet</h4> 
	    	<i>October 2014</i><br><br>
	    	Hostbasket/Telenet Hosting admin panel
	    	<br><br>
	    	<ul>
	    	<li>UX Design</li>
	    	</ul>
	    	  
	    </div>
	    <br style="clear:left">
	</section>

	<section class="project">
			<figure><img src="projectobh.jpg"></figure>           
	    <div class="content">
	    
	    	<h4>Onderwijscentrum Brussel</h4> 
	    	<i>June 2014</i><br><br>
	    	Samenmetouders.be, an online toolkit for coaches to use in a school environment
	    	<br><br>
	    	<ul>
	    	<li>Design</li>
	    	<li>Clientside Development<br>(HTML, CSS, AngularJS, Sass, Grunt)</li>
	    	<li>Serverside Development<br>(PHP, Drupal 7 + custom modules)</li>
	    	</ul>
	    	  
	    </div>
	    <br style="clear:left">
	</section>

	<section class="project">
		<figure><a href="http://www.showix.be" target="_blank"><img src="showix.jpg"></a></figure>           
	    <div class="content">
	    
	    	<h4><a href="http://www.showix.be" target="_blank">Showix</a></h4> 
	    	<i>March 2013</i><br><br>
	    	A configuration tool.
	    	<br><br>
	    	<ul>
	    	<li>Clientside Development<br>(HTML, CSS, jQuery, Backbone)</li>
	    	<li>Serverside Development<br>(PHP, Drupal 7 + custom modules)</li>
	    	</ul>
	    	  
	    </div>
	    <br style="clear:left">
	</section>
	
	<section class="project">
		<figure><a href="http://www.diversiteitinactie.be" target="_blank"><img src="diva.png"></a></figure>           
	    <div class="content">
	    
	    	<h4><a href="http://www.diversiteitinactie.be">Doe Diversiteit</a><br>(via UGent)</h4> 
	    	<i>October 2012</i><br><br>
	    	An informative website on how to handle diversity in a school environment + practical tools.
	    	<br><br>
	    	<ul>
	    	<li>Design</li>
	    	<li>Clientside Development<br>(HTML, Skeleton, CSS, jQuery)
	    	</li><li>Serverside Development<br>(PHP, Drupal 7 + custom modules)</li>
	    	</ul>
	    	  
	    </div>
	    <br style="clear:left">
	</section>
	
	<section class="project">
		<figure><a href="http://www.tomorrowstartsnow.be" target="_blank"><img src="pringles.png"></a></figure>           
	    <div class="content">
	    
	    	<h4><a href="http://www.tomorrowstartsnow.be" target="_blank">Pringles</a><br>(via Porter Novelli)</h4> 
	    	<i>October 2012</i><br><br>
	    	Private event website with photo gallery archive and possibility to create an online poster.
	    	<br><br>
	    	<ul>
	    	<li>Design</li>
	    	<li>Clientside Development<br>(HTML, CSS, jQuery)
	    	</li><li>Development<br>(PHP, Imagemagick, Drupal 7 + custom modules)</li>
	    	</ul>
	    	  
	    </div>
	     <br style="clear:left">
	</section>
	
	<section class="project">
		<figure><a href="http://www.odilon.be" target="_blank"><img src="odilon.png"></a></figure>           
	    <div class="content">
	    
	    	<h4><a href="http://www.odilon.be" target="_blank">Odilon</a><br>(via Maister)</h4> 
	    	<i>August 2012</i><br><br>
	    	Product website for wine entrepreneur. Daily updated pricelist (XLS &amp; PDF) based on website database.
	    	<br><br>
	    	<ul>
	    	<li>Project Management</li>
	    	<li>Clientside Development (HTML, CSS, jQuery)
	    	</li><li>Serverside Development (PHP, Drupal + custom modules, TCPDF, PHPExcel)</li>
	    	</ul>
	    	  
	    </div>
	     <br style="clear:left">
	</section>
	
	<section class="project">
		<figure><a href="http://waelkens.eu/" target="_blank"><img src="waelkens.png"></a></figure>           
	    <div class="content">
	    
	    	<h4><a href="http://waelkens.eu/" target="_blank">Waelkens</a><br/>(via Maister)</h4> 
	    	<i>November 2011</i><br><br>
	    	Product website with "request estimate"-module.
	    	<br><br>
	    	<ul>
	    	<li>Project Management</li>
	    	<li>Clientside Development (KnockoutJS, jQuery, HTML, CSS)</li>
	    	<li>Serverside Development (Drupal + custom modules)</li>
	    	</ul>
	    	  
	    </div>
	     <br style="clear:left">
	</section>
	
	<section class="project">
		<figure><a href="http://www.ki-prplatform.com" target="_blank"><img src="knauf.png"></a></figure>           
	    <div class="content">
	    
	    	<h4><a href="http://www.ki-prplatform.com" target="_blank">Knauf Insulation</a><br>(via Porter Novelli)</h4> 
	    	<i>December 2011</i><br><br>
	    	Private platform with tools &amp; information on Knauf PR. Different user roles and permissions, custom mailingmodule, photo gallery and private user account page with document management.
	    	Add on = Customizable analysis module which converts inputted data to statistical representation (Google Charts) + ability to choose custom parameters.
	    	<br><br>
	    	<ul>
	    	<li>Design</li>
	    	<li>Clientside Development (HTML, CSS, Google Chart Tools API)</li>
	    	<li>Serverside Development (Drupal + custom modules)</li>
	    	</ul>
	    	  
	    </div>
	     <br style="clear:left">
	</section>
	
	<section class="project">
		<figure><a href="http://www.bizzmine.com" target="_blank"><img src="bizzmine.png"></a></figure>           
	    <div class="content">
	    
	    	<h4><a href="http://www.bizzmine.com" target="_blank">Bizzmine</a><br></h4> 
	    	<i>November 2011</i><br><br>
	    	Online app for contact management, document sharing, calendar, to-dos and other tasks.
	    	<br><br>
	    	<ul>
	    	<li>Consulting</li>
	    	<li>UX Design<br>(Marketing website + product UX design)</li>
	    	</ul>
	    	  
	    </div>
	     <br style="clear:left">
	</section>
	
	<section class="project">
		<figure><img src="henrii.png"></figure>           
	    <div class="content">
	    
	    	<h4>Henrii</h4> 
	    	<i>October 2011</i><br><br>
	    	Minimalistic portfolio website for woodcarving artist.
	    	<br><br>
	    	<ul>
	    	<li>Design</li>
	    	<li>Project Management</li>
	    	<li>Clientside Development (jQuery, HTML, CSS)</li>
	    	</ul>
	    	  
	    </div>
	     <br style="clear:left">
	</section>
	
	<section class="project">
		<figure><a href="http://www.inextremis.be" target="_blank"><img src="pageinextremis.png"></a></figure>           
	    <div class="content">
	    
	    	<h4><a href="http://www.inextremis.be" target="_blank">In Extremis</a></h4> 
	    	<i>September 2011</i><br><br>
	    	Portfolio website.
	    	<br><br>
	    	<ul>
	    	<li>Design</li>
	    	<li>Project Management</li>
	    	<li>Front- and Serverside Development (jQuery, Drupal, HTML, CSS)</li>
	    	</ul>
	    	  
	    </div>
	     <br style="clear:left">
	</section>
	
	<section class="project">
		<figure><a href="http://46.20.125.91/~wwwbutte/" target="_blank"><img src="butte.png"></a></figure>           
	    <div class="content">
	    
	    	<h4><a href="http://46.20.125.91/~wwwbutte/" target="_blank">Butterslides</a></h4> 
	    	<i>June 2011</i><br><br>
	    	Portfolio website for Slide deck design firm. Experimenting with jQuery/CSS.
	    	<br><br>
	    	<ul>
	    	<li>Design</li>
	    	<li>Project Management</li>
	    	<li>Clientside Development (jQuery, Drupal, HTML, CSS)</li>
	    	</ul>
	    	  
	    </div>
	     <br style="clear:left">
	</section>
	
	<section class="project">
		<figure><a href="http://www.supermodular.be/spock" target="_blank"><img src="modular.png"></a></figure>           
	    <div class="content">
	    
	    	<h4><a href="http://www.supermodular.be/spock" target="_blank">Modular</a><br>(via Maister)</h4> 
	    	<i>August 2010</i><br><br>
	    	Mini website for product launch.
	    	<br><br>
	    	<ul>
	    	<li>Design</li>
	    	<li>Development (Actionscript 3)</li>
	    	</ul>
	    	  
	    </div>
	     <br style="clear:left">
	</section>

</div>

<canvas width="100%" height="100%"></canvas>
			
<?php include('../footer.php'); ?>
