'use strict'

module.exports = function(grunt) {
    require('time-grunt')(grunt)
    require('jit-grunt')(grunt)
    require('load-grunt-tasks')(grunt)

    grunt.initConfig({
      watch: {
        php: {
          files: ['./**/*.php']
        },
        grunt: {
          files: ['gruntfile.js']
        },
        sass: {
          files: 'css/*.scss',
          tasks: ['sass:dev']
        },
        bower: {
          files: ['assets/components/*', 'bower.json'],
          tasks: ['wiredep']
        }
      },
      sass: {
        dev: {
          files: {
            'css/main.css': 'css/main.scss'
            //'css/bootstrap.css' : 'css/bootstrap.scss',
          }
        }
      },
      browserSync: {
          dev: {
              bsFiles: {
                  src: [
                    './**/*.php',
                    'assets/**',
                    'content/**'
                  ]
              },
              options: {
                  proxy: '127.0.0.1:8010', //our PHP server
                  port: 8080, // our new port
                  open: false,
                  watchTask: true,
                  logLevel: "debug"
              }
          }
      },
      cssmin: {
        target: {
          files: [{
            expand: true,
            cwd: 'css',
            src: ['*.css', '!*.min.css'],
            dest: 'assets/css',
            ext: '.min.css'
          }]
        }
      },
      php: {
          dev: {
              options: {
                  port: 8010,
                  base: '.',
                  silent: true
              }
          }
      }
    });

    grunt.registerTask('default', ['php','cssmin','browserSync', 'watch']);
};
